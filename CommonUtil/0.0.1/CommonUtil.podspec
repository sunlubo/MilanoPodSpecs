Pod::Spec.new do |s|
  s.name         = "CommonUtil"
  s.version      = "0.0.1"
  s.summary      = "common utils"
  s.homepage     = "https://git.oschina.net/sunlubo/CommonUtil"
  s.license      = { :type => "Apache", :file => "License" }
  s.author       = { "sunlubo" => "sunlubo@milanosoft.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://git.oschina.net/sunlubo/CommonUtil.git", :tag => s.version  }
  s.source_files  = "Sources/"
end
