Pod::Spec.new do |s|
  s.name          = 'MilanoSDK'
  s.version       = '1.0.1'
  s.summary       = 'milanosoft ios sdk'
  s.homepage      = 'https://git.oschina.net/sunlubo/MilanoSDK'
  s.license       = { :type => 'Apache', :file => 'License' }
  s.author        = { 'sunlubo' => 'sunlubo@milanosoft.com' }
  s.platform      = :ios, '8.0'
  s.source        = { :git => 'https://git.oschina.net/sunlubo/MilanoSDK.git', :tag => s.version }
  s.source_files  = 'Sources/**/*.{swift,m,h}'

  # 依赖项
  s.dependency 'Alamofire'
  s.dependency 'Moya'
  s.dependency 'SwiftyJSON'
  s.dependency 'PromiseKit'
  s.dependency 'SnapKit'
end
