Pod::Spec.new do |s|
  s.name          = 'MilanoLogger'
  s.version       = '0.0.1'
  s.summary       = 'logger'
  s.homepage      = 'https://git.oschina.net/sunlubo/MilanoLogger'
  s.license       = { :type => 'Apache', :file => 'License' }
  s.author        = { 'sunlubo' => 'sunlubo@milanosoft.com' }
  s.platform      = :ios, '8.0'
  s.source        = { :git => 'https://git.oschina.net/sunlubo/MilanoLogger.git', :tag => s.version }
  s.source_files  = 'Sources/*.swift'
  s.dependency    'MilanoNetwork'
end
