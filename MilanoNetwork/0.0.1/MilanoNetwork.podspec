Pod::Spec.new do |s|
  s.name          = 'MilanoNetwork'
  s.version       = '0.0.1'
  s.summary       = 'network'
  s.homepage      = 'https://git.oschina.net/sunlubo/MilanoNetwork'
  s.license       = { :type => 'Apache', :file => 'License' }
  s.author        = { 'sunlubo' => 'sunlubo@milanosoft.com' }
  s.platform      = :ios, '8.0'
  s.source        = { :git => 'https://git.oschina.net/sunlubo/MilanoNetwork.git', :tag => s.version }
  s.source_files  = 'Sources/'
end
